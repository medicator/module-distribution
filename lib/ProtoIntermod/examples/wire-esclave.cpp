/**
 * @file wire-esclave.cpp
 * @brief Test de la communication I²C en mode esclave avec la bibliothèque
 * Wire.
 * Ce test vérifie le bon fonctionnement de la réception de chaînes de
 * caractères d'un maître et de l'envoi de chaînes de caractères à un maître.
 *
 * @target ATmega328P
 */

#include <Arduino.h>
#include <Wire.h>

//! Taille du tampon de lecture des données reçues du maître (sans le dernier
//! caractère '\0').
static const size_t TAILLE_TAMPON = 32;
//! Tampon de lecture des données reçue du maître.
char tampon[TAILLE_TAMPON + 1];

//! Adresse I²C de l'esclave auquel ce maître s'adresse.
static const uint8_t ADRESSE_ESCLAVE = 15;

/**
 * @brief Fonctions appelée lorsque l'esclave I²C a reçu des données.
 * Elle lit ces données et les imprime sur le port sériel.
 *
 */
void onReceive(
    //! Nombre d'octets reçus
    int bytes) {
    // Lire tous les octets reçus et les placer dans un tampon.
    size_t i = 0;
    while (Wire.available() != 0 && i < TAILLE_TAMPON) {
        tampon[i] = Wire.read();
        i++;
    }
    // Protéger le tampon.
    tampon[i] = 0;

    // Se débarrasser de toutes les données supplémentaires.
    while (Wire.read() != -1) {
    }

    Serial.println(tampon);
}

/**
 * @brief Fonctions appelée lorsque l'esclave I²C doit envoyer des données au
 * maître.
 * Elle ne fait que répondre la chaîne de caractères "Bonjour le monde"
 * au maître.
 *
 */
void onRequest() {
    Wire.write("Bonjour le monde!");
}

void setup() {
    Serial.begin(9600);

    // Initialiser le module I²C en tant qu'esclave.
    Wire.begin(ADRESSE_ESCLAVE);
    Wire.onReceive(onReceive);
    Wire.onRequest(onRequest);
}

void loop() {
}
