/**
 * @file wire-maitre.cpp
 * @brief Test de la communication I²C en mode maître avec un esclave avec la
 * bibliothèque Wire.
 * Ce test vérifie le bon fonctionnement de l'envoi de
 * chaînes de caractères ainsi que de la réception de chaînes de caractères.
 *
 * @target ATmega328P
 */

#include <Arduino.h>
#include <Wire.h>

//! Taille du tampon de lecture des données de l'esclave (sans son dernier
//! caractère '\0').
static const size_t TAILLE_TAMPON = 32;
//! Tampon de lecture des données de l'esclave.
char tampon[TAILLE_TAMPON + 1];

//! Adresse I²C de l'esclave auquel ce maître s'adresse.
static const uint8_t ADRESSE_ESCLAVE = 15;

void setup() {
    Serial.begin(9600);

    // Initialiser le module I²C en tant que maître.
    Wire.begin();
}

void loop() {
    // Test d'envoi.
    Wire.beginTransmission(ADRESSE_ESCLAVE);
    Wire.print("Test d'envoi de caractères");
    Wire.endTransmission();
    delay(1000);

    // Test de lecture.
    Wire.requestFrom(ADRESSE_ESCLAVE, TAILLE_TAMPON);
    size_t i;
    for (i = 0; i < TAILLE_TAMPON; i++) {
        tampon[i] = Wire.read();
    }
    tampon[i] = 0;
    Serial.println(tampon);

    delay(1000);
}
