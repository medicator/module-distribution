/**
 * @file TGestionnaireDistribution.cpp
 * @brief Définition de TGestionnaireDistribution
 */

#include "TGestionnaireDistribution.h"
#include <Arduino.h>

//! Niveau logique d'un bouton lorsqu'il est appuyé.
static const bool NIVEAU_BOUTON_APPUYE = false;

//! Temps de déplacement de la glissière en ms à partir duquel la glissière est
//! considérée comme coincée.
static const uint32_t TEMPS_DEPLACEMENT_ERREUR = 1500; // 1.5s

//! Délai obligatoire en ms entre les transition d'un état statique de la
//! glissière (ed2Recharge ou ed2Distribution) à un état mobile
//! (ed2DeplacementRecharge ou ed2DeplacementDistribution).
static const uint32_t DELAI_INTER_DEPLACEMENTS = 2000; // 2s

/**
 * @brief Initialise un gestionnaire de distribution avec les capteurs et
 * actionneurs donnés
 * @note Il faut obligatoirement appeler #begin avant n'importe quelle autre
 * méthode de cette classe après le constructeur
 */
TGestionnaireDistribution::TGestionnaireDistribution(
    //! Glissière de distribution de médicaments
    TGlissiere &glissiere,
    //! Glissière obstruée
    TCapteurObstruction &glissiereObstruee,
    //! DEL à faire clignotter lorsqu'un médicament est disponible
    TRefDel<TDel> delMedicDispo,
    //! DEL à faire clignotter lorsqu'une erreur survient
    TRefDel<TDelClignotteTemp> delErreur,
    //! Numéro de la broche du bouton de distribution
    int brocheBouton) :
    FEtat(ed2DeplacementRecharge),
    FMedicamentsADistribuer(0), FGlissiere(glissiere),
    FGlissiereObstruee(glissiereObstruee), FDelMedicDispo(delMedicDispo),
    FDelErreur(delErreur), FBrocheBouton(brocheBouton), FFinDeplacement(-1),
    DemandeDistribution(nullptr) {
}

/**
 * @brief Charge l'état du gestionnaire de distribution à partir du EEPROM et
 * initialise la broche du bouton de distribution
 *
 * @retval 0 Aucune erreur n'est survenue
 * @retval 1 L'allocation d'un espace mémoire n'a pas pu être obtenu
 * @retval 2 Les données dans l'espace mémoire n'ont pas pu être lues
 * @retval 3 L'espace mémoire n'était pas intègre, mais de nouvelles données par
 * défaut intègres ont pues y être écrites
 * @retval 4 L'espace mémoire n'était pas intègre, et de nouvelles données par
 * défaut intègres n'ont pas pues y être écrites
 */
int TGestionnaireDistribution::begin(
    //! Gestionnaire du EEPROM utilisé pour obtenir une allocation du EEPROM
    TGestionnaireEEPROM &gestionnaireEEPROM) {
    // Initialiser la broche du bouton de distribution.
    pinMode(FBrocheBouton, INPUT);

    // Restaurer l'état de la glissière.
    int codeErreur = 1;
    uint8_t commandeGlissiere = TGlissiere::COMMANDE_FERMER;
    if (gestionnaireEEPROM.Allouer(sizeof(FEtat), FAllocation) == 0) {
        if (FAllocation.EstIntegre()) {
            if (FAllocation.Lire(FEtat) == 0) {
                codeErreur = 0;

                if (FEtat == ed2Distribution ||
                    FEtat == ed2DeplacementDistribution) {
                    commandeGlissiere = TGlissiere::COMMANDE_OUVRIR;
                }
            } else {
                codeErreur = 2;
            }
        } else {
            if (FAllocation.Ecrire(FEtat) == 0) {
                codeErreur = 3;
            } else {
                codeErreur = 4;
            }
        }
    }
    FGlissiere.Commander(commandeGlissiere);

    return codeErreur;
}

/**
 * @brief Contrôle la glissière pour distribuer des médicaments lorsque
 * nécessaire et demandé
 *
 * @return Un code d'erreur
 * @retval 1 La glissière devrait être fermée pour pouvoir se recharger, mais ce
 * n'est pas le cas
 * @retval 2 Il n'y a aucun médicament à distribuer
 * @retval 3 La glissière devrait être ouverte pour pouvoir distribuer un
 * médicament, mais ce n'est pas le cas
 * @retval 4 La glissière ne peut pas être refermée si elle est obstruée
 * @retval 5 La glissière a pris trop de temps à se déplacer vers sa position
 * pour se recharger
 * @retval 6 La glissière a pris trop de temps à se déplacer vers sa position
 * pour distribuer un médicament
 * @retval 7 Impossible d'écrire dans la mémoire non volatile
 * @retval 8 La distribution d'un médicament a été refusée par le module de
 * contrôle ou le module de contrôle n'est pas connecté au module de
 * distribution.
 */
int TGestionnaireDistribution::loop() {
    // Mettre à jour l'heure de fin de déplacement.
    if (FFinDeplacement != -1 &&
        (millis() - FFinDeplacement) > DELAI_INTER_DEPLACEMENTS) {
        FFinDeplacement = -1;
    }

    // Vérifier l'état, le mettre à jour et détecter les erreurs.
    TEtatDistribution nouvelEtat = FEtat;
    int codeErreur = 0;
    if (FEtat == ed2Recharge) {
        if (FGlissiere.Etat() != egFermee) {
            FGlissiere.Commander(TGlissiere::COMMANDE_FERMER);
            codeErreur = 1;
        } else if (digitalRead(FBrocheBouton) == NIVEAU_BOUTON_APPUYE &&
                   FFinDeplacement == -1) {
            if (FMedicamentsADistribuer > 0) {
                // Demander la permission de distribuer un médicament si une
                // fonction pour effectuer cette demande a été configurée.
                bool effectuerDistribution = true;
                if (DemandeDistribution != nullptr) {
                    effectuerDistribution = DemandeDistribution();
                }

                if (effectuerDistribution) {
                    // Débuter la distribution du médicament.
                    FGlissiere.Commander(TGlissiere::COMMANDE_OUVRIR);
                    nouvelEtat = ed2DeplacementDistribution;

                    // Décrémenter le nombre de médicaments à distribuer.
                    FMedicamentsADistribuer--;
                } else {
                    codeErreur = 8;
                }
            } else {
                codeErreur = 2;
            }
        }
    } else if (FEtat == ed2Distribution) {
        if (FGlissiere.Etat() != egOuverte) {
            FGlissiere.Commander(TGlissiere::COMMANDE_OUVRIR);
            codeErreur = 3;
        } else if (digitalRead(FBrocheBouton) == NIVEAU_BOUTON_APPUYE &&
                   FFinDeplacement == -1) {
            if (FGlissiereObstruee.Obstrue()) {
                codeErreur = 4;
            } else {
                FGlissiere.Commander(TGlissiere::COMMANDE_FERMER);
                nouvelEtat = ed2DeplacementRecharge;
            }
        }
    } else if (FEtat == ed2DeplacementRecharge) {
        if (FGlissiere.Etat() == egFermee) {
            nouvelEtat = ed2Recharge;
            FFinDeplacement = millis();
        } else if (FGlissiere.TempsDeplacement() >= TEMPS_DEPLACEMENT_ERREUR) {
            codeErreur = 5;
        }
    } else if (FEtat == ed2DeplacementDistribution) {
        if (FGlissiere.Etat() == egOuverte) {
            nouvelEtat = ed2Distribution;
            FFinDeplacement = millis();
        } else if (FGlissiere.TempsDeplacement() >= TEMPS_DEPLACEMENT_ERREUR) {
            codeErreur = 6;
        }
    }
    if (nouvelEtat != FEtat) {
        FEtat = nouvelEtat;
        if (FAllocation.Ecrire(FEtat) != 0) {
            codeErreur = 7;
        }
    }

    // Afficher l'erreur survenue.
    if (codeErreur != 0) {
        FDelErreur.Del().FaireClignotter();
    }

    // Faire clignotter la DEL s'il reste au moins un médicament à distribuer.
    // Sinon, l'éteindre.
    FDelMedicDispo.Del().Etat(FMedicamentsADistribuer > 0 ? edClignotte
                                                          : edEteinte);

    return codeErreur;
}

/**
 * @brief Change le nombre de médicaments à distribuer pour le nombre donné.
 * Le nombre donné n'est pas additionné au nombre actuel de médicaments à
 * distribuer, il le remplace.
 * @note Pour annuler la distribution de médicaments, appeler cette méthode
 * avec `0` comme paramètre
 *
 */
void TGestionnaireDistribution::DistribuerMedicaments(
    //! Nouveau nombre de médicaments à distribuer.
    uint8_t medicaments) {
    FMedicamentsADistribuer = medicaments;
}

/**
 * @brief Retourne le nombre de médicaments restants à distribuer
 *
 * @return Le nombre de médicaments restants à distribuer
 */
uint8_t TGestionnaireDistribution::MedicamentsADistribuer() {
    return FMedicamentsADistribuer;
}
