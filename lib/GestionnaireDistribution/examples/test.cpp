/**
 * @file test.cpp
 * @brief Programme de test de la distribution de médicaments
 * @target ATmega328P
 */

// Configuration de TimerInterrupt
#define USE_TIMER_1 true
#define USE_TIMER_2 true
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false

#include <Arduino.h>
#include <TDel.h>
#include <TDelClignotteTemp.h>
#include <TGestionnaireDistribution.h>
#include <TGestionnaireEEPROM.h>
#include <TGlissiere.h>
#include <TGroupeCapteursObstruction.h>
#include <TVerrou.h>

//! Messages d'erreur pour la méthode #TGestionnaireDistribution::begin
static const char *MSG_ERREUR_GESTIONNAIRE_DISTRIBUTION_BEGIN[] = {
    "L'allocation d'un espace mémoire n'a pas pu être obtenu",
    "Les données dans l'espace mémoire n'ont pas pu être lues",
    "L'espace mémoire n'était pas intègre, mais de nouvelles données par "
    "défaut intègres ont pues y être écrites",
    "L'espace mémoire n'était pas intègre, et de nouvelles données par défaut "
    "intègres n'ont pas pues y être écrites",
};

//! Messages d'erreur pour la méthode #TGestionnaireDistribution::loop
static const char *MSG_ERREUR_GESTIONNAIRE_DISTRIBUTION_LOOP[] = {
    "La glissière devrait être fermée pour pouvoir se recharger, mais ce n'est "
    "pas le cas",
    "Il n'y a aucun médicament à distribuer",
    "La glissière devrait être ouverte pour pouvoir distribuer un médicament, "
    "mais ce n'est pas le cas",
    "La glissière ne peut pas être refermée si elle est obstruée",
    "La glissière a pris trop de temps à se déplacer vers sa position pour se "
    "recharger",
    "La glissière a pris trop de temps à se déplacer vers sa position pour "
    "distribuer un médicament",
    "Impossible d'écrire dans la mémoire non volatile",
};

static const int BROCHE_DIRECTION_OUVRIR = 9;
static const int BROCHE_CAPTEUR_OUVERT = 10;
static const int BROCHE_DIRECTION_FERMER = 11;
static const int BROCHE_CAPTEUR_FERME = 12;
TGlissiere glissiere(BROCHE_DIRECTION_OUVRIR, BROCHE_DIRECTION_FERMER,
                     BROCHE_CAPTEUR_OUVERT, BROCHE_CAPTEUR_FERME);

//! Niveau retourné par un capteur à partir duquel il considère qu'il est
//! obstrué
static const uint8_t NIVEAU_OBSTRUCTION = 150; // ~2V

//! Capteur d'obstruction de la glissière.
TCapteurObstruction capteurGlissiereObstrue(A3, NIVEAU_OBSTRUCTION, 2000);

//! DEL pour signaler qu'un médicament est disponible.
TRefDel<TDel> delMedicDispo(6);

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(8);

//! Broche du bouton de distribution.
static const int BROCHE_BOUTON_DISTRIBUTION = 13;

//! Gestionnaire de la distribution de médicaments.
TGestionnaireDistribution gestionnaireDistribution(glissiere,
                                                   capteurGlissiereObstrue,
                                                   delMedicDispo, delErreur,
                                                   BROCHE_BOUTON_DISTRIBUTION);

//! Gestionnaire du EEPROM.
TGestionnaireEEPROM gestionnaireEEPROM(EEPROM);

void setup() {
    Serial.begin(9600);

    EEPROM.begin();

    delMedicDispo.Del().Etat(edEteinte);
    delErreur.Del().NbClignottements(3);
    TGestionnaireDels::begin(500, ITimer1);

    capteurGlissiereObstrue.begin();
    glissiere.begin();

    int erreurInitGestionnaireDist =
        gestionnaireDistribution.begin(gestionnaireEEPROM);
    if (erreurInitGestionnaireDist != 0) {
        Serial.println(MSG_ERREUR_GESTIONNAIRE_DISTRIBUTION_BEGIN
                           [erreurInitGestionnaireDist - 1]);
    }
}

void loop() {
    if (Serial.available()) {
        long int nbMedicaments = Serial.readStringUntil('\n').toInt();
        gestionnaireDistribution.DistribuerMedicaments(nbMedicaments);
        Serial.print("Nombre de médicaments à distribuer changé à ");
        Serial.println(nbMedicaments);
    }

    capteurGlissiereObstrue.Lire();

    int erreurGestionnaireDist = gestionnaireDistribution.loop();
    if (erreurGestionnaireDist != 0) {
        Serial.println(
            MSG_ERREUR_GESTIONNAIRE_DISTRIBUTION_LOOP[erreurGestionnaireDist -
                                                      1]);
    }
}
