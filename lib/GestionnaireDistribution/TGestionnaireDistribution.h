/**
 * @file TGestionnaireDistribution.h
 * @brief Déclaration de TGestionnaireDistribution et TEtatDistribution
 */

#ifndef TGestionnaireDistribution_H
#define TGestionnaireDistribution_H

#include <TCapteurObstruction.h>
#include <TDelClignotteTemp.h>
#include <TGestionnaireEEPROM.h>
#include <TGlissiere.h>
#include <stdint.h>

//! Représente l'état d'un TGestionnaireDistribution
enum TEtatDistribution {
    //! Le distributeur est dans position où il peut recharger un médicament
    ed2Recharge,
    //! Le distributeur est dans position où il peut recharger un médicament
    ed2Distribution,

    //! Le distributeur se déplace vers la position où il va se recharger
    ed2DeplacementRecharge,
    //! Le distributeur se déplace vers la position où il va distribuer un
    //! médicament
    ed2DeplacementDistribution,

    ed2Premier = ed2Recharge,
    ed2Dernier = ed2DeplacementDistribution,
    ed2Defaut = ed2DeplacementRecharge,
};

//! Gère la distribution de médicaments
class TGestionnaireDistribution {
private:
    //! État de cet objet.
    //! Cet état est enregistré chaque fois qu'il change
    //! et il est restauré dans begin.
    TEtatDistribution FEtat;

    //! Nombre de médicaments en attente d'être distribués
    uint8_t FMedicamentsADistribuer;

    //! Glissière pour distribuer des médicaments
    TGlissiere &FGlissiere;

    //! Capteur d'obstruction de la glissière en position de distribution
    TCapteurObstruction &FGlissiereObstruee;

    //! DEL à faire clignotter lorsqu'un médicament est disponible
    TRefDel<TDel> FDelMedicDispo;
    //! DEL à faire clignotter lorsqu'une erreur survient
    TRefDel<TDelClignotteTemp> FDelErreur;

    //! Broche du bouton de distribution
    int FBrocheBouton;

    //! Allocation dans le EEPROM où sauvegarder de manière
    //! non volatile l'état de cet objet
    TAllocationEEPROM FAllocation;

    //! Heure de la fin d'un déplacement.
    //! Ceci permet d'avoir un délai pour permettre au client de prendre son
    //! médicament de la glissière et de permettre à la glissière de se
    //! recharger.
    //! Si ceci est à `-1`, le délai est écoulé et la glissière peut être
    //! déplacée dans une autre direction.
    int64_t FFinDeplacement;

public:
    //! Fonction appelée juste avant d'effectuer la distribution d'un
    //! médicament.
    //! Si sa valeur est `nullptr`, le médicament est distribué.
    //! Si sa valeur n'est pas `nullptr`, et qu'elle retourne `true` lorsqu'elle
    //! est appelée, le médicament est distribué.
    //! Si sa valeur n'est pas `nullptr`, et qu'elle retourne `false`
    //! lorsqu'elle est appelée, le médicament n'est pas distribué.
    bool (*DemandeDistribution)();

    TGestionnaireDistribution(TGlissiere &glissiere,
                              TCapteurObstruction &glissiereObstruee,
                              TRefDel<TDel> delMedicDispo,
                              TRefDel<TDelClignotteTemp> delErreur,
                              int brocheBouton);

    int begin(TGestionnaireEEPROM &gestionnaireEEPROM);
    int loop();

    void DistribuerMedicaments(uint8_t medicaments);
    uint8_t MedicamentsADistribuer();
};

#endif /* TGestionnaireDistribution_H */
