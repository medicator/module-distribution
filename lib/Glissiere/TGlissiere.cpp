/**
 * @file Glissiere.h
 * @brief Déclaration de TGlissiere
 */

#include "TGlissiere.h"
#include <Arduino.h>

//! Niveau logique d'un capteur de fin de course lorsqu'il est enclenché
#define FIN_COURSE_ATTEINTE LOW

//! Niveau logique à envoyer sur une broche de direction pour faire aller le
//! moteur dans la direction donnée
#define DIRECTION_ACTIVEE HIGH
//! Niveau logique à envoyer sur une broche de direction pour ne plus que le
//! moteur aille dans cette direction
#define DIRECTION_DESACTIVEE LOW

/**
 * @brief Initialise une nouvelle glissière.
 * @note Il faut appeler la méthode begin
 * avant d'appeler n'importe quelle autre méthode de la glissière.
 */
TGlissiere::TGlissiere(
    //! Broche pour contrôler l'ouverture de la glissière.
    int directionOuvrir,
    //! Broche pour contrôler la fermeture de la glissière.
    int directionFermer,
    //! Broche pour vérifier si la glissière est ouverte.
    int capteurOuvert,
    //! Broche pour vérifier si la glissière est ouverte.
    int capteurFerme) :
    FDirectionOuvrir(directionOuvrir),
    FDirectionFermer(directionFermer), FCapteurOuvert(capteurOuvert),
    FCapteurFerme(capteurFerme), FCommande(COMMANDE_NEUTRE),
    FDebutDeplacement(0) {
}

/**
 * @brief Initialise toutes les broches de la glissière et
 * l'arrête en utilisant la commande neutre
 * @note Cette méthode doit être appelée avant n'importe quelle autre méthode
 * après le constructeur
 */
void TGlissiere::begin() {
    // Initialiser les broches de direction en sortie
    pinMode(FDirectionOuvrir, OUTPUT);
    pinMode(FDirectionFermer, OUTPUT);

    // Initialiser les broches des capteurs de fin de course en entrée
    pinMode(FCapteurOuvert, INPUT);
    pinMode(FCapteurFerme, INPUT);

    // Fermer le distributeur.
    Commander(COMMANDE_NEUTRE);
}

/**
 * @brief Calcul et retourne le nombre de millisecondes depuis le début du
 * déplacement. Si la glissière a atteint sa position, la valeur retournée est
 * 0.
 *
 * @return uint32_t Le nombre de millisecondes depuis le début du déplacement
 */
uint32_t TGlissiere::TempsDeplacement() {
    // Si la glissière est en déplacement alors
    //   Retourner le nombre de millisecondes écoulées depuis le début du
    //     déplacement à la routine appelante.
    // Sinon
    //   Retourner 0 à la routine appelante.
    // Fin du si
    TEtatGlissiere etat = Etat();
    return etat == egOuverture || etat == egFermeture
               ? millis() - FDebutDeplacement
               : 0;
}

/**
 * @brief Commande à la glissière de se rendre à une position donnée
 * @note Si la commande donnée est invalide, la glissière sera mise à la
 * position neutre.
 */
void TGlissiere::Commander(
    //! Direction de la glissière
    //! @see TGlissiere::COMMANDE_NEUTRE
    //! @see TGlissiere::COMMANDE_OUVRIR
    //! @see TGlissiere::COMMANDE_FERMER
    uint8_t commande) {
    // Déterminer le niveau logique des broches.
    bool niveauOuverture = DIRECTION_DESACTIVEE;
    bool niveauFermeture = DIRECTION_DESACTIVEE;
    if (commande == COMMANDE_OUVRIR) {
        niveauOuverture = DIRECTION_ACTIVEE;
    } else if (commande == COMMANDE_FERMER) {
        niveauFermeture = DIRECTION_ACTIVEE;
    } else {
        commande = COMMANDE_NEUTRE;
    }

    // Mettre à jour la commande.
    FCommande = commande;

    // Changer le niveau logique des broches.
    digitalWrite(FDirectionOuvrir, niveauOuverture);
    digitalWrite(FDirectionFermer, niveauFermeture);

    // Mettre à jour l'heure de début du déplacement.
    FDebutDeplacement = millis();
}

/**
 * @brief Détermine et retourne l'état de la glissière
 *
 * @return TEtatGlissiere l'état de la glissière
 */
TEtatGlissiere TGlissiere::Etat() {
    TEtatGlissiere etat = egNeutre;

    if (FCommande == COMMANDE_FERMER) {
        etat = digitalRead(FCapteurFerme) == FIN_COURSE_ATTEINTE ? egFermee
                                                                 : egFermeture;
    } else if (FCommande == COMMANDE_OUVRIR) {
        etat = digitalRead(FCapteurOuvert) == FIN_COURSE_ATTEINTE ? egOuverte
                                                                  : egOuverture;
    }

    return etat;
}
