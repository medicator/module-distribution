/**
 * @file Glissiere.h
 * @brief Déclaration de TGlissiere et TEtatGlissiere
 */

#ifndef TGlissiere_H
#define TGlissiere_H

#include <stdint.h>

//! Représente l'état de la glissière
enum TEtatGlissiere : uint8_t {
    //! La glissière est à un état neutre.
    //! Il est toutefois possible qu'elle soit ouverte ou fermée.
    egNeutre,
    //! La glissière est ouverte.
    egOuverte,
    //! La glissière est en déplacement pour s'ouvrir.
    egOuverture,
    //! La glissière est fermée.
    egFermee,
    //! La glissière est en déplacement pour se fermer.
    egFermeture,

    egPremier = egNeutre,
    egDernier = egFermee,
    egDefaut = egNeutre,
};

//! Permet de contrôler la glissière pour distribuer des médicaments.
class TGlissiere {
private:
    //! Broche pour contrôler l'ouverture de la glissière (la position où elle
    //! distribue un médicament).
    const int FDirectionOuvrir;
    //! Broche pour contrôler la fermeture de la glissière (la position où elle
    //! se recharge avec un médicament).
    const int FDirectionFermer;

    //! Broche du capteur de fin de course qui est activé lorsque la glissière
    //! est en position ouverte.
    const int FCapteurOuvert;
    //! Broche du capteur de fin de course qui est activé lorsque la glissière
    //! est en position fermée.
    const int FCapteurFerme;

    //! Position où la glissière doit se rendre.
    uint8_t FCommande;
    //! Heure du début du déplacement de la glissière.
    uint32_t FDebutDeplacement;

public:
    //! Valeur de la commande pour que la glissière ne bouge pas.
    static const uint8_t COMMANDE_NEUTRE = 0;
    //! Valeur de la commande pour ouvrir la glissière.
    static const uint8_t COMMANDE_OUVRIR = 1;
    //! Valeur de la commande pour fermer la glissière.
    static const uint8_t COMMANDE_FERMER = 2;

    TGlissiere(int directionOuvrir, int directionFermer, int capteurOuvert,
               int capteurFerme);

    void begin();

    uint32_t TempsDeplacement();

    void Commander(uint8_t commande);
    TEtatGlissiere Etat();
};

#endif /* TGlissiere_H */
