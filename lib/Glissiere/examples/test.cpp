/**
 * @file main.cpp
 * @brief Test la glissière.
 * Envoyer `0` sur le port sériel pour mettre la glissière à neutre, `1` pour l'
 * ouvrir et `2` pour la fermer.
 *
 * @target ATmega328P
 */
#include <Arduino.h>
#include <TGlissiere.h>

static const int BROCHE_DIRECTION_OUVRIR = 9;
static const int BROCHE_CAPTEUR_OUVERT = 10;

static const int BROCHE_DIRECTION_FERMER = 11;
static const int BROCHE_CAPTEUR_FERME = 12;

TGlissiere glissiere(BROCHE_DIRECTION_OUVRIR, BROCHE_DIRECTION_FERMER,
                     BROCHE_CAPTEUR_OUVERT, BROCHE_CAPTEUR_FERME);

void setup() {
    Serial.begin(9600);

    // Initialiser la glissière.
    glissiere.begin();
}

static const char *ETATS_GLISSIERE[] = {"Neutre", "Ouverte", "Ouverture",
                                        "Fermee", "Fermeture"};

void loop() {
    // Si l'utilisateur a entré du texte.
    if (Serial.available() != 0) {
        // Lire le nombre entré par l'utilisateur.
        int commande = Serial.readStringUntil('\n').toInt();

        glissiere.Commander(commande);
    }

    Serial.print(ETATS_GLISSIERE[glissiere.Etat()]);
    Serial.print(": ");
    Serial.println(glissiere.TempsDeplacement());
}
