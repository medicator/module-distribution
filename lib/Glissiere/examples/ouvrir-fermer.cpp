/**
 * @file ouvrir-fermer.cpp
 * @brief En la faisant constamment ouvrir et fermer.
 * @target ATmega328P
 */
#include <Arduino.h>
#include <TGlissiere.h>

static const int BROCHE_DIRECTION_OUVRIR = 9;
static const int BROCHE_CAPTEUR_OUVERT = 10;

static const int BROCHE_DIRECTION_FERMER = 11;
static const int BROCHE_CAPTEUR_FERME = 12;

TGlissiere glissiere(BROCHE_DIRECTION_OUVRIR, BROCHE_DIRECTION_FERMER,
                     BROCHE_CAPTEUR_OUVERT, BROCHE_CAPTEUR_FERME);

void setup() {
    Serial.begin(9600);

    // Initialiser la glissière.
    glissiere.begin();
}

void loop() {
    glissiere.Commander(TGlissiere::COMMANDE_OUVRIR);
    Serial.println("Ouvrir");
    while (glissiere.TempsDeplacement() < 1000 &&
           glissiere.Etat() != egOuverte) {
    }

    glissiere.Commander(TGlissiere::COMMANDE_FERMER);
    Serial.println("Fermer");
    while (glissiere.TempsDeplacement() < 1000 &&
           glissiere.Etat() != egFermee) {
    }
}
