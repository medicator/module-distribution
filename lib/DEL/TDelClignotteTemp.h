/**
 * @file TDelClignotteTemp.h
 * @brief Déclaration de TDelClignotteTemp
 */

#ifndef TDelClignotteTemp_H
#define TDelClignotteTemp_H

#include "TDel.h"

//! Permet de faire clignotter une DEL un certain nombre de fois
//! à chaque fois que la méthode FaireClignotter
class TDelClignotteTemp : private TDel {
    friend class TRefDel<TDelClignotteTemp>;

private:
    //! Nombre de fois que la DEL doit clignotter
    //! avant de s'éteindre lors d'une erreur
    uint8_t FNbClignottements;
    //! Nombre de clignottement restant à effectuer multiplié par 2.
    //! Lorsque ce nombre est 0, la DEL n'est plus en train de clignotter.
    //! Ce nombre est constamment décrémenter dans MaJEtat.
    uint16_t FNbClignottementsRestants;

    TDelClignotteTemp(int broche);

    // Supprimer le constructeur de copies.
    TDelClignotteTemp(TDel &del) = delete;

public:
    //! Nombre de clignottements par défaut.
    //! Il peut être changé en appelant NbClignottements(uint8_t).
    static const uint8_t NB_CLIGNOTTEMENTS_DEFAUT = 5;

    void FaireClignotter();
    bool Clignotte();

    void NbClignottements(uint8_t nbClignottements);
    uint8_t NbClignottements();

protected:
    void MaJEtat() override;
};

#endif // TDelClignotteTemp_H
