/**
 * @file test.cpp
 * @brief Programme de test des classes TDel et TDelClignotteTemp.
 */

#include <Arduino.h>

// Configuration de TimerInterrupt
#define USE_TIMER_1 true
#define USE_TIMER_2 true
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false

#include <TDel.h>
#include <TDelClignotteTemp.h>

//! DEL pour signaler qu'un médicament est disponible.
TRefDel<TDel> delMedicDispo(6);

//! DEL pour afficher l'état du réseau.
TRefDel<TDel> delCellulaire(7);

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(8);

void setup() {
    Serial.begin(9600);

    delMedicDispo.Del().Etat(edClignotte);
    delCellulaire.Del().Etat(edClignotte);
    TGestionnaireDels::begin(1000, ITimer1);
}

void loop() {
    Serial.print('.');
    delay(100);

    if (Serial.available()) {
        Serial.read();
        delErreur.Del().FaireClignotter();
    }
}
