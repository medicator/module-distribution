/**
 * @file TVerrou.cpp
 * @brief Implémentation de TVerrou
 */

#include "TVerrou.h"
#include <Arduino.h>

//! Valeur du PWM pour verrouiller le verrou.
#define PWM_VERROUILLER 0
//! Valeur du PWM pour déverrouiller le verrou.
#define PWM_DEVEROUILLER (255 / 2) // rapport de cycle de 50%

/**
 * @brief Initialise un nouveau verrou avec la broche suivante.
 *
 */
TVerrou::TVerrou(
    //! Broche pour contrôler le verrou
    int broche,
    //! Délai en ms pendant lequel le verrou doit être déverrouillé
    //! avant d'être automatiquement reverrouillé.
    //! Cette valeur peut être modifiée après le constructeur en
    //! modifiant la propriété DelaiDeverrouillage
    uint32_t delaiDeverrouillage) :
    FBroche(broche),
    DelaiDeverrouillage(delaiDeverrouillage) {
}

/**
 * @brief Initialise le verrou et l'éteint
 * @note Il faut appeler cette méthode avant d'appeler n'importe quelle autre
 * méthode après le constructeur
 */
void TVerrou::begin() {
    // Configurer la broche du verrou.
    pinMode(FBroche, OUTPUT);

    // Verrouiller le verrou.
    Etat(VERROUILLE);
}

/**
 * @brief Permet de reverrouiller le le verrou après que le délai de
 * déverrouillage est écoulé
 * @note Il est important d'appeler cette méthode périodiquement, comme dans la
 * méthode loop, sinon le verrou ne sera pas automatiquement reverrouillé.
 */
void TVerrou::loop() {
    // Si le verrou est déverrouillé et que le délai de déverrouillage est
    // déverrouillé
    if (FEtat == DEVERROUILLE &&
        (millis() - FDebutDeverrouillage) >= DelaiDeverrouillage) {
        // Reverrouiller le verrou.
        Etat(VERROUILLE);
    }
}

/**
 * @brief Retourne l'état actuel du verrou
 *
 * @return VERROUILLE le verrou est verrouillé
 * @return DEVERROUILLE le verrou est déverrouillé
 */
bool TVerrou::Etat() {
    return FEtat;
}

/**
 * @brief Change l'état du verrou pour celui donné
 *
 * @param etat
 */
void TVerrou::Etat(
    //! Nouvel état du verrou
    //! @see VERROUILLE
    //! @see DEVERROUILLE
    bool etat) {
    // Changer la variable d'état.
    FEtat = etat;

    analogWrite(FBroche,
                etat == VERROUILLE ? PWM_VERROUILLER : PWM_DEVEROUILLER);

    // Si le verrou a été déverrouillé.
    if (etat == DEVERROUILLE) {
        // Assigner l'heure actuelle au début du déverrouillage.
        FDebutDeverrouillage = millis();
    }
}

/**
 * @brief Verrouille le verrou pendant le délai configuré
 * @note Ceci n'est qu'un raccourci pour la méthode Etat(VERROUILLE)
 */
void TVerrou::Deverrouiller() {
    Etat(DEVERROUILLE);
}
