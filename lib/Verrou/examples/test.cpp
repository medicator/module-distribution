/**
 * @file test.cpp
 * @brief Programme de test de TVerrou
 * @target ATmega328P
 */
#include "TVerrou.h"
#include <Arduino.h>

#define PIN 5
#define DELAI_DEVERROUILLAGE 10000 // 10s

TVerrou verrou(PIN, DELAI_DEVERROUILLAGE);

void setup() {
    Serial.begin(9600);

    verrou.begin();
}

void loop() {
    // Mettre à jour l'état du verrou.
    verrou.loop();

    if (Serial.read() != -1) {
        verrou.Deverrouiller();
    }

    // Afficher l'état du verrou.
    Serial.println(verrou.Etat() ? "Verrouillé" : "Déverrouillé");

    delay(100);
}
