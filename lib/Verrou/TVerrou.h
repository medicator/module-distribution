/**
 * @file TVerrou.h
 * @brief Déclaration de TVerrou
 */

#ifndef TVerrou_H
#define TVerrou_H

#include <stdint.h> // uint32_t

//! Contrôle un verrou en PWM avec un cycle de travail de 50%. Le verrou est
//! verrouillé par défaut et ne se verrouille que pendant un interval spécifié.
//! s'il est.
//! @note Le verrou doit être verrouillé sur un niveau logique bas
class TVerrou {
private:
    //! Broche pour contrôler le verrou
    const int FBroche;
    //! Broche pour l'état du verrou
    bool FEtat;

    //! Heure à laquelle le verrou a été déverrouillé
    //! Cette variable permet de savoir quand reverrouiller le verrou
    uint32_t FDebutDeverrouillage;

public:
    //! Valeur de l'état du verrou lorsqu'il est verrouillé
    static const bool VERROUILLE = true;
    //! Valeur de l'état du verrou lorsqu'il est déverrouillé
    static const bool DEVERROUILLE = false;

    //! Délai en ms pendant lequel le verrou doit être déverrouillé
    //! avant d'être automatiquement reverrouillé.
    uint32_t DelaiDeverrouillage;

    TVerrou(int broche, uint32_t delaiDeverrouillage);

    void begin();
    void loop();

    bool Etat();
    void Etat(bool etat);

    void Deverrouiller();
};

#endif // TVerrou_H
