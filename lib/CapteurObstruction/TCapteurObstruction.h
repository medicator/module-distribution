/**
 * @file TCapteurObstruction.h
 * @brief Déclaration de TCapteurObstruction
 */

#ifndef TCapteurObstruction_H
#define TCapteurObstruction_H

#include <stdint.h> // uint*_t

//! Permet de lire la valeur d'un capteur d'obstruction analogique.
//! Il utilise un délai configurable pour s'assurer que le capteur d'obstruction
//! est bien découvert.
class TCapteurObstruction {
private:
    //! Broche du capteur
    int FBroche;
    //! Niveau à partir duquel le capteur d'obstruction considère qu'il est
    //! obstrué
    uint8_t FNiveauObstruction;
    //! Délai entre le moment ou on a capter que le capteur n'était pas obstrué
    //! et celui où.
    uint32_t FDelaiSansObstructions;

    //! Heure du début du moment à partir duquel il n'était pas obstrué.
    uint32_t FDebutNonObstrue;
    //! Dernière lecture du capteur
    //! @see OBSTRUE
    //! @see NON_OBSTRUE
    bool FDerniereLecture;

public:
    //! Valeur retournée par Obstrue et ObstrueImmediatement lorsque le
    //! capteur est obstrué.
    static const bool OBSTRUE = true;
    //! Valeur retournée par Obstrue et ObstrueImmediatement lorsque le
    //! capteur n'est pas obstrué.
    static const bool NON_OBSTRUE = false;

    TCapteurObstruction(int broche, uint8_t niveauObstruction,
                        uint32_t delaiSansObstructions);
    TCapteurObstruction();

    void ChangerBroche(int broche);
    void ChangerNiveauObstruction(uint8_t niveauObstruction);
    void ChangerDelaiSansObstructions(uint32_t delaiSansObstructions);

    void begin();
    void Lire();

    bool Obstrue();
    bool ObstrueImmediatement();

    uint32_t DelaiSansObstruction();
};

#endif /* TCapteurObstruction_H */
