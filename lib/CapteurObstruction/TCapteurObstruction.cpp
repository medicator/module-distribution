/**
 * @file TCapteurObstruction.cpp
 * @brief Implémentation de TCapteurObstruction
 */

#include "TCapteurObstruction.h"
#include <Arduino.h>

/**
 * @brief Initialise un nouveau capteur d'obstruction
 * @note Il faut absolument appeler la méthode begin avant d'appeler n'importe
 * quelle autre méthode après le constructeur.
 */
TCapteurObstruction::TCapteurObstruction(
    //! Broche analogique pour lire la valeur du capteur.
    int broche,
    //! Niveau minimum retourné par le capteur à partir duquel considéré qu'il
    //! est obstrué.
    uint8_t niveauObstruction,
    //! Délai à partir du moment ou le capteur détecte qu'il n'est pas obstrué
    //! et celui ou il considère qu'il n'est vraiment pas obstrué
    uint32_t delaiSansObstructions) :
    FBroche(broche),
    FNiveauObstruction(niveauObstruction),
    FDelaiSansObstructions(delaiSansObstructions), FDerniereLecture(OBSTRUE) {
}

/**
 * @brief Initialise un nouveau capteur d'obstruction avec comme broche A0, un
 * niveau d'obstruction de 150 et un délai sans obstructions de 10s.
 * @note Si ces paramètres par défaut ne vous conviennent pas, utiliser les
 * méthodes Changer* pour le reconfigurer.
 * @note Il faut absolument appeler la méthode begin avant d'appeler n'importe
 * quelle autre méthode après le constructeur.
 */
TCapteurObstruction::TCapteurObstruction() :
    TCapteurObstruction(A0, 150, 10000) {
}

/**
 * @brief Change la broche du capteur pour celle donnée
 * @note Il faut absolument appeler begin après avoir appeler cette fonction
 * pour initialiser la broche donnée en entrée analogique, si ce n'est pas déjà
 * fait.
 */
void TCapteurObstruction::ChangerBroche(
    //! Nouvelle broche
    int broche) {
    FBroche = broche;
}

/**
 * @brief Change le niveau d'obstruction du capteur pour celui donné
 *
 */
void TCapteurObstruction::ChangerNiveauObstruction(
    //! Nouveau niveau d'obstruction
    uint8_t niveauObstruction) {
    FNiveauObstruction = niveauObstruction;
}

/**
 * @brief Change le délai sans obstructions pour celui donné
 *
 */
void TCapteurObstruction::ChangerDelaiSansObstructions(
    //! Nouveau délai sans obstructions
    uint32_t delaiSansObstructions) {
    FDelaiSansObstructions = delaiSansObstructions;
}

/**
 * @brief Configure la broche du capteur d'obstruction
 * @note Il faut appeler cette méthode avant n'importe quelle autre méthode de
 * cette classe après le constructeur.
 */
void TCapteurObstruction::begin() {
    pinMode(FBroche, INPUT);
}

/**
 * @brief Prend une lecture du capteur
 * @note Ceci est à appeler dans la fonction loop
 */
void TCapteurObstruction::Lire() {
    bool nouvelleLecture = ObstrueImmediatement();

    // Si le capteur était obstrué et ne l'est plus
    if (FDerniereLecture == OBSTRUE && nouvelleLecture == NON_OBSTRUE) {
        // Changer l'heure du début de non obstruction à maintenant.
        FDebutNonObstrue = millis();
    }

    // Enregistrer la nouvelle lecture.
    FDerniereLecture = nouvelleLecture;
}

/**
 * @brief Retourne si le capteur est obstrué ou non en tenant
 * compte du délai d'obstruction.
 *
 * @return Si le capteur est obstrué
 * @retval TCapteurObstruction::OBSTRUE le capteur est obstrué
 * @retval TCapteurObstruction::NON_OBSTRUE le capteur n'est pas obstrué
 */
bool TCapteurObstruction::Obstrue() {
    // Lire une valeur du capteur.
    Lire();

    // Initialiser le résultat à la valeur de la dernière lecture.
    bool resultat = FDerniereLecture;

    // Si le capteur est immédiatement obstrué, mais que le délai de
    // non obstruction n'est pas dépassé.
    if (resultat == NON_OBSTRUE &&
        (millis() - FDebutNonObstrue) < FDelaiSansObstructions) {
        // Le capteur est obstrué.
        resultat = OBSTRUE;
    }

    // Retourner le résultat.
    return resultat;
}

/**
 * @brief Retourne si le le capteur est obstrué dans l'immédiat.
 * Ceci ignore le délai de non obstruction.
 *
 * @return Si le capteur est actuellement obstrué
 * @retval TCapteurObstruction::OBSTRUE le capteur est obstrué
 * @retval TCapteurObstruction::NON_OBSTRUE le capteur n'est pas obstrué
 */
bool TCapteurObstruction::ObstrueImmediatement() {
    // Retourner si la valeur lue du capteur est
    // plus grande ou égale au niveau d'obstruction.
    return analogRead(FBroche) >= FNiveauObstruction;
}

/**
 * @brief Retourne le temps écoulé depuis que le capteur n'est pas obstrué
 *
 * @return uint32_t Le temps écoulé depuis que le capteur n'est pas obstrué.
 */
uint32_t TCapteurObstruction::DelaiSansObstruction() {
    // Lire une valeur du capteur.
    Lire();

    // Si le capteur est immédiatement obstrué
    //   Retourner 0.
    // Sinon
    //   Retourner le délai écoulé depuis que le capteur n'est pas obstrué.
    // Fin du si
    return FDerniereLecture == OBSTRUE ? 0 : (millis() - FDebutNonObstrue);
}
