/**
 * @file TCapteursNiveau.h
 * @brief Déclaration de TCapteursNiveau
 */

#ifndef TCapteursNiveau_H
#define TCapteursNiveau_H

#include "TGroupeCapteursObstruction.h"
#include <TDelClignotteTemp.h>

//! Détecte le niveau de médicament à l'aide d'un groupe
//! de capteurs d'obstruction
class TCapteursNiveau : public TGroupeCapteursObstruction {
private:
    //! DEL utilisée pour signaler une erreur de
    //! notification de changement de niveau.
    TRefDel<TDelClignotteTemp> FDelErreur;

    //! Si la lecture précédente des capteurs d'obstruction indique
    //! que le réservoir est à un niveau critique.
    bool FNiveauCritique;
    //! Si la notification du niveau de médicament a été envoyée.
    bool FNotifEnvoyee;

public:
    //! Fonction appelée lorsque le niveau de médicament change.
    //! Si cette méthode retourne `false`, cette méthode sera constamment
    //! rappelée jusqu'à ce qu'elle retourne `true`.
    //!
    //! @return Si la notification a pue être envoyée avec succès
    //! @retval true La notification a pue être envoyée avec succès
    //! @retval true Une erreur est survenue lors de l'envoi de la notification
    bool (*NotifierChangementNiveau)(
        //! Si le niveau critique a été atteint
        bool niveauCritique);

    TCapteursNiveau(const int *brochesCapteurs, size_t nbCapteurs,
                    uint8_t niveauObstruction, uint32_t delaiSansObstructions,
                    TRefDel<TDelClignotteTemp> delErreur);

    void loop();
};

#endif /* TCapteursNiveau_H */
