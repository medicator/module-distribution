/**
 * @file TGroupeCapteursObstruction.h
 * @brief Déclaration de TGroupeCapteurObstruction
 */

#ifndef TGroupeCapteursObstruction_H
#define TGroupeCapteursObstruction_H

#include "TCapteurObstruction.h"
#include <stddef.h> // size_t

//! Groupe de capteurs d'obstructions.
//! Tant qu'un des capteurs du groupe est obstrué, le groupe de capteurs est
//! considéré comme obstrué.
class TGroupeCapteursObstruction {
    //! Liste des capteurs du groupe
    TCapteurObstruction *FCapteurs;
    //! Nombre de capteurs dans le groupe
    const size_t FNbCapteurs;

public:
    TGroupeCapteursObstruction(const int *brochesCapteurs, size_t nbCapteurs,
                               uint8_t niveauObstruction,
                               uint32_t delaiSansObstructions);
    ~TGroupeCapteursObstruction();

    void begin();
    void Lire();

    bool ObstruesImmediatement();
    bool Obstrues();

    uint32_t DelaiSansObstruction();

    TCapteurObstruction *Capteurs();
    size_t NbCapteurs();
};

#endif /* TGroupeCapteursObstruction_H */
