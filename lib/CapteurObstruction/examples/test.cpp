/**
 * @file test.cpp
 * @brief Programme de test de TCapteurObstruction et TGroupeCapteursObstruction
 * @target ATmega328P
 */
#include <Arduino.h>
#include <TDelClignotteTemp.h>
#include <TGroupeCapteursObstruction.h>

//! Niveau retourné par un capteur à partir duquel il considère qu'il est
//! obstrué
#define NIVEAU_OBSTRUCTION 150 // ~2V

//! Délai sans obstruction pour un capteur à partir duquel il considère qu'il
//! n'est réelement pas obstrué
#define DELAI_SANS_OBSTRUCTION 10000 // 10s

//! Capteur d'obstruction.
TCapteurObstruction capteur(A3, NIVEAU_OBSTRUCTION, DELAI_SANS_OBSTRUCTION);

//! Broches du groupe de capteurs d'obstruction.
static const int BROCHES_GROUPE_CAPTEURS[] = {A1, A2};
//! Groupe de capteurs d'obstruction.
TGroupeCapteursObstruction groupeCapteurs(BROCHES_GROUPE_CAPTEURS, 2,
                                          NIVEAU_OBSTRUCTION,
                                          DELAI_SANS_OBSTRUCTION);

void setup() {
    Serial.begin(9600);

    // Initialiser le capteur.
    capteur.begin();

    // Initialiser le groupe de capteurs.
    groupeCapteurs.begin();
}

void loop() {
    // Afficher l'état du capteur seul.
    Serial.println("Solo: ");
    Serial.print(capteur.Obstrue() ? 'x' : 'o');
    Serial.print(", ");
    Serial.print(capteur.ObstrueImmediatement() ? 'x' : 'o');
    Serial.print(", ");
    Serial.println(capteur.DelaiSansObstruction());

    Serial.println("Groupe: ");
    // Afficher l'état de chaque capteur du groupe.
    for (size_t i = 0; i < groupeCapteurs.NbCapteurs(); i++) {
        TCapteurObstruction &capteur = groupeCapteurs.Capteurs()[i];

        Serial.print('#');
        Serial.print(i);
        Serial.print(": ");

        Serial.print(capteur.Obstrue() ? 'x' : 'o');
        Serial.print(", ");
        Serial.print(capteur.ObstrueImmediatement() ? 'x' : 'o');
        Serial.print(", ");
        Serial.println(capteur.DelaiSansObstruction());
    }

    // Afficher l'état du groupe de capteurs.
    Serial.print(groupeCapteurs.Obstrues() ? 'x' : 'o');
    Serial.print(", ");
    Serial.print(groupeCapteurs.ObstruesImmediatement() ? 'x' : 'o');
    Serial.print(", ");
    Serial.println(groupeCapteurs.DelaiSansObstruction());

    Serial.println();

    delay(500);
}
