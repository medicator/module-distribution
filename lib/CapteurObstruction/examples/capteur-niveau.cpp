/**
 * @file test.cpp
 * @brief Programme de test de TCapteurObstruction et TGroupeCapteursObstruction
 * @target ATmega328P
 */

// Configuration de TimerInterrupt
#define USE_TIMER_1 true
#define USE_TIMER_2 true
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false

#include <Arduino.h>
#include <TCapteursNiveau.h>
#include <TimerInterrupt.h>

//! Niveau retourné par un capteur à partir duquel il considère qu'il est
//! obstrué
#define NIVEAU_OBSTRUCTION 150 // ~2V

//! Délai sans obstruction pour un capteur à partir duquel il considère qu'il
//! n'est réelement pas obstrué
#define DELAI_SANS_OBSTRUCTION 10000 // 10s

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(8);

//! Broches du groupe de capteurs d'obstruction.
static const int BROCHES_GROUPE_CAPTEURS[] = {A1, A2};
//! Groupe de capteurs d'obstruction.
TCapteursNiveau capteursNiveau(BROCHES_GROUPE_CAPTEURS, 2, NIVEAU_OBSTRUCTION,
                               DELAI_SANS_OBSTRUCTION, delErreur);

//! Valeur retournée par notifierChangementNiveau.
bool notificationReussie = true;

/**
 * @brief Affiche le niveau du capteur de médicament lorsqu'il change
 *
 * @return Si la notification a pue être envoyée avec succès
 * @retval true La notification a pue être envoyée avec succès
 * @retval true Une erreur est survenue lors de l'envoi de la notification
 */
bool notifierChangementNiveau(
    //! Si le niveau critique a été atteint
    bool niveauCritique) {
    Serial.println(
        niveauCritique
            ? "Le niveau de médicament est critique"
            : "Le niveau de médicament n'a pas atteint le seuil critique");

    return notificationReussie;
}

void setup() {
    // Initialiser la communication sérielle.
    Serial.begin(9600);

    // Initialiser les DELs.
    delErreur.Del().NbClignottements(3);
    TGestionnaireDels::begin(500, ITimer1);

    // Initialiser les capteurs de niveau.
    capteursNiveau.begin();
    capteursNiveau.NotifierChangementNiveau = notifierChangementNiveau;
}

void loop() {
    if (Serial.read() >= 0) {
        notificationReussie = !notificationReussie;
    }

    capteursNiveau.loop();
}
