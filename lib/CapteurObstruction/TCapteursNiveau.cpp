/**
 * @file TCapteursNiveau.cpp
 * @brief Implémentation de TCapteursNiveau
 */

#include "TCapteursNiveau.h"

/**
 * @brief Initialise un nouveau capteur de niveau avec les propriétés
 * données pour le TGroupeCapteursObstruction et la DEL d'erreur donnée
 *
 */
TCapteursNiveau::TCapteursNiveau(
    //! Broches des capteurs
    const int *brochesCapteurs,
    //! Nombre de capteurs dans le groupe
    size_t nbCapteurs,
    //! Niveau d'obstruction des capteurs
    uint8_t niveauObstruction,
    //! Délai à partir du moment où le capteur détecte qu'il n'est pas
    //! obstrué et celui ou il considère qu'il n'est vraiment pas obstrué
    uint32_t delaiSansObstructions,
    //! DEL à faire clignoter lorsqu'une erreur de communication survient
    TRefDel<TDelClignotteTemp> delErreur) :
    TGroupeCapteursObstruction(brochesCapteurs, nbCapteurs, niveauObstruction,
                               delaiSansObstructions),
    FDelErreur(delErreur), FNiveauCritique(false), FNotifEnvoyee(true) {
}

/**
 * @brief Surveille le niveau de médicament et appel
 * NotifierChangementNiveau lorsqu'il change
 *
 */
void TCapteursNiveau::loop() {
    bool niveauCritique = Obstrues() == TCapteurObstruction::NON_OBSTRUE;

    // Si le niveau de médicament a changé
    if (FNiveauCritique != niveauCritique) {
        FNiveauCritique = niveauCritique;
        FNotifEnvoyee = false;
    }

    //! Si la notification pour le niveau de médicament n'a pas été envoyée
    if (!FNotifEnvoyee) {
        if (NotifierChangementNiveau != nullptr) {
            FNotifEnvoyee = NotifierChangementNiveau(niveauCritique);
        } else {
            FNotifEnvoyee = false;
        }

        // Si la notification pour le niveau de médicaments
        // n'a pas pu être envoyée
        if (!FNotifEnvoyee) {
            FDelErreur.Del().FaireClignotter();
        }
    }
}
