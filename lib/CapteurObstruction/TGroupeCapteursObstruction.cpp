/**
 * @file TGroupeCapteursObstruction.cpp
 * @brief Implémentation de TGroupeCapteurObstruction
 */

#include "TGroupeCapteursObstruction.h"
#include <stdlib.h> // malloc

/**
 * @brief Initialise un groupe de capteurs d'obstructions avec les broches
 * données, le niveau d'obstruction donné et le délai de non obstruction donné
 */
TGroupeCapteursObstruction::TGroupeCapteursObstruction(
    //! Broches des capteurs
    const int *brochesCapteurs,
    //! Nombre de capteurs dans le groupe
    size_t nbCapteurs,
    //! Niveau d'obstruction des capteurs
    uint8_t niveauObstruction,
    //! Délai à partir du moment ou le capteur détecte qu'il n'est pas obstrué
    //! et celui ou il considère qu'il n'est vraiment pas obstrué
    uint32_t delaiSansObstructions) :
    FNbCapteurs(nbCapteurs) {
    if (brochesCapteurs == nullptr) {
        throw "L'argument brochesCapteurs passé à TGroupeCapteursObstruction "
              "ne peut pas être nul.";
    }

    // Allouer l'espace pour la liste de capteurs.
    FCapteurs = new TCapteurObstruction[nbCapteurs];

    // Changer les propriétés de chaque capteur.
    for (size_t i = 0; i < FNbCapteurs; i++) {
        TCapteurObstruction &capteur = FCapteurs[i];
        capteur.ChangerBroche(brochesCapteurs[i]);
        capteur.ChangerNiveauObstruction(niveauObstruction);
        capteur.ChangerDelaiSansObstructions(delaiSansObstructions);
    }
}

/**
 * @brief Détruit le groupe de capteurs d'obstruction et tous les capteurs
 * d'obstruction qu'il contient
 *
 */
TGroupeCapteursObstruction::~TGroupeCapteursObstruction() {
    // Supprimer la liste de capteurs.
    delete[] FCapteurs;
}

/**
 * @brief Initialise tous les capteurs du groupe
 *
 */
void TGroupeCapteursObstruction::begin() {
    // Initialiser chaque capteur d'obstruction.
    for (size_t i = 0; i < FNbCapteurs; i++) {
        FCapteurs[i].begin();
    }
}

/**
 * @brief Effectue une lecture de chaque capteur
 *
 */
void TGroupeCapteursObstruction::Lire() {
    for (size_t i = 0; i < FNbCapteurs; i++) {
        FCapteurs[i].Lire();
    }
}

/**
 * @brief Retourne si les capteurs sont obstrués sans tenir compte de leur
 * délai d'obstruction
 *
 * @return Si le capteur est obstrué
 * @retval TCapteurObstruction::OBSTRUE le capteur est obstrué
 * @retval TCapteurObstruction::NON_OBSTRUE le capteur n'est pas obstrué
 */
bool TGroupeCapteursObstruction::ObstruesImmediatement() {
    // Initialiser le résultat comme un capteur non obstrué.
    bool resultat = TCapteurObstruction::NON_OBSTRUE;

    // Pour chaque capteur tant qu'aucun capteur obstruer n'a été recontré
    for (size_t i = 0;
         i < FNbCapteurs && resultat == TCapteurObstruction::NON_OBSTRUE; i++) {
        if (FCapteurs[i].ObstrueImmediatement() ==
            TCapteurObstruction::OBSTRUE) {
            resultat = TCapteurObstruction::OBSTRUE;
        }
    }

    return resultat;
}

/**
 * @brief Retourne si les capteurs sont obstrués en tenant compte de leur
 * délai d'obstruction
 *
 * @return Si le capteur est obstrué
 * @retval TCapteurObstruction::OBSTRUE le capteur est obstrué
 * @retval TCapteurObstruction::NON_OBSTRUE le capteur n'est pas obstrué
 */
bool TGroupeCapteursObstruction::Obstrues() {
    // Initialiser le résultat comme un capteur non obstrué.
    bool resultat = TCapteurObstruction::NON_OBSTRUE;

    // Pour chaque capteur
    for (size_t i = 0; i < FNbCapteurs; i++) {
        if (FCapteurs[i].Obstrue() == TCapteurObstruction::OBSTRUE) {
            resultat = TCapteurObstruction::OBSTRUE;
        }
    }

    return resultat;
}

/**
 * @brief Retourne le temps écoulé depuis qu'aucun capteur n'est obstrué
 *
 * @return uint32_t le temps écoulé depuis qu'aucun capteur n'est obstrué
 */
uint32_t TGroupeCapteursObstruction::DelaiSansObstruction() {
    // Initialiser le résultat au plus grand délai possible.
    uint32_t resultat = UINT32_MAX;

    // Pour chaque capteur
    for (size_t i = 0; i < FNbCapteurs; i++) {
        // Si le délai du capteur est inférieur au résultat.
        uint32_t delai = FCapteurs[i].DelaiSansObstruction();
        if (delai < resultat) {
            // Assigner le délai du capteur au résultat.
            resultat = delai;
        }
    }

    return resultat;
}

/**
 * @brief Retourne la liste de capteurs d'obstruction
 *
 * @return TCapteurObstruction* Retourne la liste de capteurs d'obstruction,
 * cette valeur n'est jamais nulle
 */
TCapteurObstruction *TGroupeCapteursObstruction::Capteurs() {
    return FCapteurs;
}

/**
 * @brief Retourne le nombre de capteurs d'obstruction
 *
 * @return size_t Le nombre de capteurs d'obstruction du groupe
 */
size_t TGroupeCapteursObstruction::NbCapteurs() {
    return FNbCapteurs;
}
