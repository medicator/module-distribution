/**
 * @file test.cpp
 * @brief Programme de test de TGestionnaireEEPROM
 * @target ATmega328P
 */

#include <Arduino.h>
#include <TGestionnaireEEPROM.h>

//! Gestionnaire de EEPROM.
TGestionnaireEEPROM gestionnaireEEPROM(EEPROM);

//! Réinitialise le microcontrôleur en déplaçant le pointeur d'instructions à
//! l'adresse 0
//! @see https://www.instructables.com/id/two-ways-to-reset-arduino-in-software/
void (*reinitialiser)() = 0;

/**
 * @brief Alloue un espace mémoire, lit sa valeur, incrémente sa valeur d'un
 * incrément donné puis réécrit la valeur incrémentée dans l'espace mémoire
 *
 */
void test(
    //! Incrément à ajouter à la valeur lue avant de la réenregistrer
    int increment) {
    TAllocationEEPROM allocation;
    int erreurAllocation = gestionnaireEEPROM.Allouer(sizeof(int), allocation);
    if (erreurAllocation == 0) {
        Serial.println("Allocation réussie");

        int compteur = 0;
        int erreurLecture = allocation.Lire(compteur);
        if (erreurLecture == 0) {
            Serial.print("Lecture réussie: ");
            Serial.println(compteur);
            compteur += increment;
        } else if (erreurLecture == 4) {
            Serial.println("L'espace mémoire n'est pas intègre");
        } else {
            Serial.print("Erreur de lecture: ");
            Serial.println(erreurLecture);
        }

        int erreurEcriture = allocation.Ecrire(compteur);
        if (erreurEcriture == 0) {
            Serial.println("Écriture réussie");
        } else {
            Serial.print("Erreur d'écriture: ");
            Serial.println(erreurEcriture);
        }
    } else {
        Serial.print("Erreur d'allocation: ");
        Serial.println(erreurAllocation);
    }
    Serial.println();
}

void setup() {
    Serial.begin(9600);
    EEPROM.begin();

    for (int i = 0; i < 100; i++) {
        test(i);
    }

    Serial.println();
    delay(1000);
    reinitialiser();
}

void loop() {
}
