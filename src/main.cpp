/**
 * @file main.cpp
 * @brief Programme principal du module de distribution
 * @target ATmega328P
 */

// Configuration de TimerInterrupt
#define USE_TIMER_1 true
#define USE_TIMER_2 true
#define USE_TIMER_3 false
#define USE_TIMER_4 false
#define USE_TIMER_5 false

#include <Arduino.h>
#include <TCapteursNiveau.h>
#include <TCommunication.h>
#include <TDel.h>
#include <TDelClignotteTemp.h>
#include <TGestionnaireDistribution.h>
#include <TGestionnaireEEPROM.h>
#include <TGlissiere.h>
#include <TGroupeCapteursObstruction.h>
#include <TVerrou.h>

//! DEL pour signaler qu'un médicament est disponible.
TRefDel<TDel> delMedicDispo(6);

//! DEL signalant l'initialisation du module.
TRefDel<TDel> delInitialisation(7);

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(8);

//! Glissière de distribution de médicaments.
TGlissiere glissiere(9, 11, 10, 12);

//! Verrou de la recharge.
TVerrou verrou(5, 0);

//! Capteur d'obstruction de la glissière.
TCapteurObstruction capteurGlissiereObstrue(A3, 150, 2000);

//! Broches du groupe de capteurs d'obstruction.
static const int BROCHES_GROUPE_CAPTEURS[] = {A1, A2};
//! Groupe de capteurs d'obstruction.
TCapteursNiveau capteursNiveau(BROCHES_GROUPE_CAPTEURS, 2, 150, 10000,
                               delErreur);

//! Gestionnaire de la distribution de médicaments.
TGestionnaireDistribution gestionnaireDistribution(glissiere,
                                                   capteurGlissiereObstrue,
                                                   delMedicDispo, delErreur,
                                                   13);

//! Gestionnaire du EEPROM.
TGestionnaireEEPROM gestionnaireEEPROM(EEPROM);

//! Gestionnaire de communication.
TCommunication communication =
    TCommunication::Instance(Wire, gestionnaireDistribution, verrou, 10);

/**
 * @brief Notifie le module de contrôle de la distribution d'un médicament et
 * retourne s'il a accepté qu'un médicament soit distribué
 *
 * @return Si un médicament peut être distribué
 * @retval true Le module de contrôle à accepté qu'un médicament soit distribué
 * @retval false La communication avec le module de contrôle a échouée ou il a
 * refusé de distribué un médicament
 */
bool demandeDistribution() {
    return communication.NotifierDistribution() == 0;
}

/**
 * @brief Affiche le niveau du capteur de médicament lorsqu'il change
 *
 * @return Si la notification a pue être envoyée avec succès
 * @retval true La notification a pue être envoyée avec succès
 * @retval true Une erreur est survenue lors de l'envoi de la notification
 */
bool notifierChangementNiveau(
    //! Si le niveau critique a été atteint
    bool niveauCritique) {
    return communication.NotifierChangementNiveauMedicament(niveauCritique) ==
           0;
}

void setup() {
    // Initialiser la communication sérielle.
    Serial.begin(9600);

    // Initialiser le EEPROM.
    EEPROM.begin();

    // Initialiser les DELs.
    delMedicDispo.Del().Etat(edEteinte);
    delErreur.Del().NbClignottements(3);
    delInitialisation.Del().Etat(edClignotte);
    TGestionnaireDels::begin(500, ITimer1);

    // Initialiser les composants matériel.
    capteurGlissiereObstrue.begin();
    glissiere.begin();
    verrou.begin();
    capteursNiveau.NotifierChangementNiveau = notifierChangementNiveau;
    capteursNiveau.begin();
    gestionnaireDistribution.DemandeDistribution = demandeDistribution;
    gestionnaireDistribution.begin(gestionnaireEEPROM);

    // Initialiser la communication.
    communication.begin();
    delInitialisation.Del().Etat(edAllumee);
}

void loop() {
    capteurGlissiereObstrue.Lire();

    verrou.loop();
    gestionnaireDistribution.loop();
    capteursNiveau.loop();
}
