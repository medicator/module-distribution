@startuml UML

class TAllocationEEPROM {
	-FEeprom : EEPROMClass*
	-FAdresse : TAdresseEEPROM
	-FTaille : TAdresseEEPROM
	-FIntegre : bool
---
	+TAllocationEEPROM()
	+Adresse() : TAdresseEEPROM
	+Taille() : TAdresseEEPROM
	+TailleTotale() : TAdresseEEPROM
	+EstAlloue() : bool
	+EstIntegre() : bool
	-Allouer(eeprom: EEPROMClass&, adresse: TAdresseEEPROM,
	        taille: TAdresseEEPROM) : int
	+Ecrire(donnees: const uint8_t*, taille: TAdresseEEPROM,
	        decalage: TAdresseEEPROM) : int
	+Ecrire(donnees: const T&, decalage: TAdresseEEPROM) : int
	+Lire(donnees: uint8_t*, taille: TAdresseEEPROM,
	      decalage: TAdresseEEPROM) : int
	+Lire(donnees: T&, decalage: TAdresseEEPROM) : int
---
	Représente un espace mémoire alloué dans le EEPROM
	dont l'intégrité est assurée par un CRC32
}

class TCapteurObstruction {
	-FDebutNonObstrue : uint32_t
	-FDelaiSansObstructions : uint32_t
	-FNiveauObstruction : uint8_t
	-FBroche : int
	-FDerniereLecture : bool
	+{static} NON_OBSTRUE : const bool
	+{static} OBSTRUE : const bool
---
	+TCapteurObstruction(broche: int, niveauObstruction: uint8_t,
	                    delaiSansObstructions: uint32_t)
	+TCapteurObstruction()
	+Obstrue() : bool
	+ObstrueImmediatement() : bool
	+DelaiSansObstruction() : uint32_t
	+ChangerBroche(broche: int)
	+ChangerDelaiSansObstructions(delaiSansObstructions: uint32_t)
	+ChangerNiveauObstruction(niveauObstruction: uint8_t)
	+Lire()
	+begin()
---
	Permet de lire la valeur d'un capteur d'obstruction analogique.
	Il utilise un délai configurable pour s'assurer que le capteur
	d'obstruction est bien découvert.
}

class TCapteursNiveau {
	-FDelErreur : TRefDel<TDelClignotteTemp>
	-FNiveauCritique : bool
	-FNotifEnvoyee : bool
---
	+TCapteursNiveau(brochesCapteurs: const int*, nbCapteurs: size_t,
	                 niveauObstruction: uint8_t,
					 delaiSansObstructions: uint32_t,
					 delErreur: TRefDel<TDelClignotteTemp>)
	+NotifierChangementNiveau : bool (*)(niveauCritique: bool)
	+loop()
---
	Détecte le niveau de médicament à l'aide d'un
	groupe de capteurs d'obstruction
}

class TCommunication {
	-{static} FInstance : static TCommunication*
	-FAdresseEsclave : uint8_t
	-FTamponEnvoiEsclave : uint8_t
	-FTamponReceptionEsclave : uint8_t
	-FTamponReceptionMaitre : uint8_t
	-FDistribution : TGestionnaireDistribution&
	-FVerrou : TVerrou&
	-FPortI2C : TwoWire&
---
	-TCommunication(portI2C: TwoWire&, distribution: TGestionnaireDistribution&,
	                verrou: TVerrou&, adresseEsclave: uint8_t)
	+{static} Instance(portI2C: TwoWire&, distribution: TGestionnaireDistribution&,
	                   verrou: TVerrou&, adresseEsclave: uint8_t) : TCommunication&
	-EnvoyerCommande(codeCommande: uint8_t, donnees: uint8_t*,
	                 tailleDonnees: uint8_t) : int
	+NotifierChangementNiveauMedicament(niveauCritique: bool) : int
	+NotifierDistribution() : int
	-GererCommande()
	-{static} OnReceive(nbOctets: int)
	-{static} OnRequest()
	+begin()
---
	Gère la communication avec le module de contrôle
}

class TDel {
	-FDelPrecedente : TDel*
	-FDelSuivante : TDel*
	-FEtat : TEtatDel
	-FBroche : const int
	-FNbReferences : uint8_t
---
	-TDel(del: TDel&)
	#TDel(broche: int)
	+~TDel()
	+Etat(etat: TEtatDel) : int
	+Etat() : int
	#MaJEtat()
---
	Permet d'allumer d'éteindre et de
	faire clignotter des
	DELs en utilisant des timers.

	Cette classe ne peut pas directement
	être instanciée. Pour l'utiliser,
	vous devez créer une TRefDel<TDel>.

	Pour avoir accès à la DEL vous pourrez
	utiliser la méthode TRefDel<TDel>::Del.
}

class TDelClignotteTemp {
	+{static} NB_CLIGNOTTEMENTS_DEFAUT : const uint8_t
	-FNbClignottementsRestants : uint16_t
	-FNbClignottements : uint8_t
---
	-TDelClignotteTemp(broche: int)
	-TDelClignotteTemp(del: TDel&)
	+Clignotte() : bool
	+NbClignottements() : uint8_t
	+FaireClignotter()
	#MaJEtat()
	+NbClignottements(nbClignottements: uint8_t)
---
	Permet de faire clignotter une DEL un
	certain nombre de fois à chaque fois
	que la méthode FaireClignotter
}

class TGestionnaireDels {
	-{static} FDerniereDel : static TDel*
	-{static} FClignottement : static bool
---
	-TGestionnaireDels()
	-{static} Isr()
	+{static} begin(periode: uint32_t, timer: TimerInterrupt&)
---
	Gère l'état d'une liste de DEL en utilisant
	un Timer pour les faire clignotter
}

class TGestionnaireDistribution {
	-FAllocation : TAllocationEEPROM
	-FGlissiereObstruee : TCapteurObstruction&
	-FEtat : TEtatDistribution
	-FGlissiere : TGlissiere&
	-FDelMedicDispo : TRefDel<TDel>
	-FDelErreur : TRefDel<TDelClignotteTemp>
	+DemandeDistribution : bool (*)()
	-FBrocheBouton : int
	-FFinDeplacement : int64_t
	-FMedicamentsADistribuer : uint8_t
---
	+TGestionnaireDistribution(glissiere: TGlissiere&,
	                           glissiereObstruee: TCapteurObstruction&,
							   delMedicDispo: TRefDel<TDel>,
							   delErreur: TRefDel<TDelClignotteTemp>,
							   brocheBouton: int)
	+begin(gestionnaireEEPROM: TGestionnaireEEPROM&) : int
	+loop() : int
	+MedicamentsADistribuer() : uint8_t
	+DistribuerMedicaments(medicaments: uint8_t)
---
	Gère la distribution de médicaments
}

class TGestionnaireEEPROM {
	-FEeprom : EEPROMClass&
	-FProchaineAllocation : TAdresseEEPROM
---
	+TGestionnaireEEPROM(eeprom: EEPROMClass&)
	+EspaceAlloue() : TAdresseEEPROM
	+EspaceRestant() : TAdresseEEPROM
	+Allouer(taille: TAdresseEEPROM,
	         allocation: TAllocationEEPROM&) : int
---
	Gère l'accès de plusieurs objets à
	différentes partie du EEPROM et permet de
	facilement s'assurer de l'intégrité de
	chacune de ces partie
}

class TGlissiere {
	-FCapteurFerme : const int
	-FCapteurOuvert : const int
	-FDirectionFermer : const int
	-FDirectionOuvrir : const int
	+{static} COMMANDE_FERMER : const uint8_t
	+{static} COMMANDE_NEUTRE : const uint8_t
	+{static} COMMANDE_OUVRIR : const uint8_t
	-FDebutDeplacement : uint32_t
	-FCommande : uint8_t
---
	+TGlissiere(directionOuvrir: int, directionFermer: int,
	            capteurOuvert: int, capteurFerme: int)
	+Etat() : char*
	+TempsDeplacement() : uint32_t
	+Commander(commande: uint8_t)
	+begin()
---
	Permet de contrôler la glissière pour
	distribuer des médicaments.
}

class TGroupeCapteursObstruction {
	-FCapteurs : TCapteurObstruction*
	-FNbCapteurs : const size_t
---
	+TGroupeCapteursObstruction(brochesCapteurs: const int*,
	                            nbCapteurs: size_t,
								niveauObstruction: uint8_t,
								delaiSansObstructions: uint32_t)
	+~TGroupeCapteursObstruction()
	+Capteurs() : TCapteurObstruction*
	+Obstrues() : bool
	+ObstruesImmediatement() : bool
	+NbCapteurs() : size_t
	+DelaiSansObstruction() : uint32_t
	+Lire()
	+begin()
---
	Groupe de capteurs d'obstructions.
	Tant qu'un des capteurs du groupe est obstrué, le groupe
	de capteurs est considéré comme obstrué.
}

class TRefDel <TTypeDel> {
	+TRefDel(broche: int)
	+TRefDel(ref: TRefDel<TTypeDel>&)
	+~TRefDel()
	+Del() : TTypeDel&
	-FDel : TTypeDel*
---
	Réference vers une TDel.
	Ceci est un pointeur à compte
	de référence d'objets de
	classe TDel

	TTypeDel: Type de la DEL,
	ceci doit être un type qui
	hérite de TDel
}

class TVerrou {
	-FEtat : bool
	-FBroche : const int
	+{static} DEVERROUILLE : const bool
	+{static} VERROUILLE : const bool
	+DelaiDeverrouillage : uint32_t
	-FDebutDeverrouillage : uint32_t
---
	+TVerrou(broche: int, delaiDeverrouillage: uint32_t)
	+Etat() : bool
	+Deverrouiller()
	+Etat(etat: bool)
	+begin()
	+loop()
---
	Contrôle un verrou en PWM avec un cycle de travail
	de 50%. Le verrou est verrouillé par défaut et ne
	se verrouille que pendant un interval spécifié.

	Le verrou doit être verrouillé sur un niveau logique bas
}

enum TEtatDel {
	edAllumee
	edClignotte
	edDernier
	edEteinte
	edInactive
	edPremier
---
	Représente l'état d'une
	DEL pour TDel
}

enum TEtatDistribution {
	ed2Defaut
	ed2DeplacementDistribution
	ed2DeplacementRecharge
	ed2Dernier
	ed2Distribution
	ed2Premier
	ed2Recharge
---
	Représente l'état d'un
	TGestionnaireDistribution
}

enum TEtatGlissiere {
	egDefaut
	egDernier
	egFermee
	egFermeture
	egNeutre
	egOuverte
	egOuverture
	egPremier
---
	Représente l'état
	de la glissière
}

/' Héritage '/

TDel <|-- TDelClignotteTemp
TGroupeCapteursObstruction <|- TCapteursNiveau

/' Possession '/

TCapteursNiveau *-- TDelClignotteTemp
TCapteursNiveau *- TRefDel
TCommunication o-- TCommunication
TGestionnaireDistribution -* TCommunication
TVerrou --* TCommunication
TDel "2" o-- TDel
TEtatDel -* TDel
TDel -o TGestionnaireDels
TGestionnaireDistribution *-- TAllocationEEPROM
TEtatDistribution --* TGestionnaireDistribution
TCapteurObstruction -* TGestionnaireDistribution
TGestionnaireDistribution *-- TDel
TGestionnaireDistribution *-- TDelClignotteTemp
TGlissiere --* TGestionnaireDistribution
TGestionnaireDistribution "2" *-- TRefDel
TGroupeCapteursObstruction o- TCapteurObstruction
TEtatGlissiere <- TGlissiere

TGestionnaireEEPROM <-- TGestionnaireDistribution

@enduml
